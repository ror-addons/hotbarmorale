HotbarMorale = {}

local oldCursorSwap = nil

function HotbarMorale.Initialize()
    
    oldCursorSwap = ActionButton.CursorSwap
    
    ActionButton.CursorSwap =
        function(self, ...)
            if Cursor.IconOnCursor() then
                if Cursor.Data.Source == Cursor.SOURCE_ACTION_LIST or
                   Cursor.Data.Source == GameData.PlayerActions.DO_ABILITY then
                   
                    -- Check if it's a morale ability on the cursor
                    local abilityData = Player.GetAbilityData(Cursor.Data.ObjectId)
                    if abilityData == nil then return end
                    
                    if abilityData.abilityType == GameData.AbilityType.MORALE then
                        -- Special case if it's a morale ability
                        
                        -- Still have to check to make sure we can modify the slot, though
                        if self:VerifySlotIsUserModifiable(ActionButton.PRINT_MODIFICATION_ATTEMPT_ERROR,                           ActionButton.MODIFICATION_TYPE_SET_DATA) == false then return end
                        
                        -- Assuming we can modify it, force the modification
                        local mySlot, slotType, slotId, slotIcon = self:GetActionData()
                        SetHotbarData (mySlot, GameData.PlayerActions.DO_ABILITY, Cursor.Data.ObjectId)
                        Cursor.Clear()
                        
                        -- Pick up the old slot data if there was something there before
                        if slotId ~= 0 then        
                            Cursor.PickUp(slotType, 0, slotId, slotIcon, Cursor.AUTO_PICKUP_ON_LBUTTON_UP)
                        end
                        
                        -- So that the button tooltip will update properly
                        self:OnMouseOver (flags, x, y)
                        
                        -- Don't run the original swap function, we don't need an extraneous error message
                        return
                    end
                end
            end
            
            -- Call the original function
            return oldCursorSwap(self, ...)
        end

end
