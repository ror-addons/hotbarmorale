<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="HotbarMorale" version="1.4" date="16/10/2009" >

		<Author name="Aiiane" email="aiiane@aiiane.net" />
		<Description text="Allows Morale abilities to be put on the standard hotbars." />
		
        <VersionSettings gameVersion="1.3.2" windowsVersion="1.0" savedVariablesVersion="1.0" />
        
        <Dependencies>
            <Dependency name="EA_ActionBars" />
        </Dependencies>
        
		<Files>
			<File name="HotbarMorale.lua" />
		</Files>
		
		<OnInitialize>
            <CallFunction name="HotbarMorale.Initialize" />
		</OnInitialize>
        
        <WARInfo>
    <Categories>
        <Category name="ACTION_BARS" />
    </Categories>
    <Careers>
        <Career name="BLACKGUARD" />
        <Career name="WITCH_ELF" />
        <Career name="DISCIPLE" />
        <Career name="SORCERER" />
        <Career name="IRON_BREAKER" />
        <Career name="SLAYER" />
        <Career name="RUNE_PRIEST" />
        <Career name="ENGINEER" />
        <Career name="BLACK_ORC" />
        <Career name="CHOPPA" />
        <Career name="SHAMAN" />
        <Career name="SQUIG_HERDER" />
        <Career name="WITCH_HUNTER" />
        <Career name="KNIGHT" />
        <Career name="BRIGHT_WIZARD" />
        <Career name="WARRIOR_PRIEST" />
        <Career name="CHOSEN" />
        <Career name= "MARAUDER" />
        <Career name="ZEALOT" />
        <Career name="MAGUS" />
        <Career name="SWORDMASTER" />
        <Career name="SHADOW_WARRIOR" />
        <Career name="WHITE_LION" />
        <Career name="ARCHMAGE" />
    </Careers>
</WARInfo>

		
	</UiMod>
</ModuleFile>
